def before_noon(hour, is_am):
    return (is_am and hour == 11) or (not is_am and hour == 12)


# Tests
print(before_noon(11, True))
print(before_noon(12, False))

